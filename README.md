# vuejs-scrimba-exercice-final

Exercice final de l'introduction à la documentation vuejs

Préview : https://vuejs-scrimba-exercice-final.netlify.app/

-   créer une page "liste article" avec l'api JSON placeholder
-   L'api récupère 100 articles : prendre seulement 20 articles
-   styliser la vue de la vignette article detail (faire comme sur preview)
-   en haut de page trois boutons de tri des articles :
    -   trier par id croissant
    -   trier par id décroissant
    -   tri random

Pour le tri des posts, il faudra passer des évènements d'un enfant vers un parent, tu auras donc besoin de l'outil "\$emit", il y a un tutoriel sur le cours openclassrooms : https://openclassrooms.com/fr/courses/6390311-creez-une-application-web-avec-vue-js/6864936-emettez-des-evenements-vers-des-composants-parents

Condition pour le tri : il devra se faire avec la méthode sort en JS

Lors du tri, il ne faut pas que les images changent : tu génère les posts une fois au chargement et ensuite tu tri ton tableau sans le modifier

Vu qu'il y a beaucoup de style, n'hésite pas à utiliser du SCSS
