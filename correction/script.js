Vue.component('app-action', {
	template: `
		<div class="actions">
			<button @click.prevent="croissant()">Tri croissant</button>
			<button @click.prevent="decroissant()">Tri décroissant</button>
			<button @click.prevent="random()">Tri random</button>
		</div>
	`,
	props: ['posts'],
	methods: {
		croissant() {
			this.$emit('order', 'croissant');
		},
		decroissant() {
			this.$emit('order', 'decroissant');
		},
		random() {
			this.$emit('order', 'random');
		},
	},
});

Vue.component('app-post', {
	template: `
		<div class="post">
			<img :src="post.image" alt="image-article" />
			<div class="id">{{post.id}}</div>
			<div class="content">
				<div class="title">{{post.title}}</div>
				<div class="description">{{post.body}}</div>
			</div>
		</div>
	`,
	props: ['post'],
});

const app = new Vue({
	el: '#app',
	data: {
		posts: [],
	},
	beforeCreate() {
		fetch('https://jsonplaceholder.typicode.com/posts')
			.then((res) => res.json())
			.then((data) => {
				data.forEach((post, index) => {
					if (index < 20) {
						post.image = `https://picsum.photos/400/400?random=${index + 1}`;

						this.posts.push(post);
					}
				});
			});
	},
	methods: {
		orderControl(payload) {
			this[payload]();
		},
		croissant() {
			this.posts = this.posts.sort((a, b) => a.id - b.id);
		},
		decroissant() {
			this.posts = this.posts.sort((a, b) => b.id - a.id);
		},
		random() {
			this.posts = this.posts.sort(() => Math.random() - 0.5);
		},
	},
});
