Vue.component("filter-button", {
  name: "filter-button",
  props: ["content", "sortingmethod"],

  template: `<button @click="emitSortEvent">{{ content }}</button>`,

  methods: {
    emitSortEvent: function () {
      this.$emit("sort-post", { sorting: this.sortingmethod });
    },
  },
});

Vue.component("post-unit", {
  name: "post-unit",

  props: ["post"],

  template: `<div class="post">
    <img :src="imageUrl" :alt="post.title" />

    <div class="post-id">
      {{post.id}}
    </div>

    <div class="post-content">

      <div class="post-title">
        {{post.title}}
      </div>

      <div class="post-body">
        {{post.body}}
      </div>

    </div>
  </div>`,

  data: function () {
    return {
      imageUrl: null,
    };
  },

  beforeCreate: function () {
    fetch("https://picsum.photos/400").then((res) => {
      this.imageUrl = res.url;
    });
  },
});

let app = new Vue({
  el: "#app",

  data: {
    posts: [],
  },

  beforeCreate: function () {
    fetch("http://jsonplaceholder.typicode.com/posts?_limit=20")
      .then((response) => response.json())
      .then((data) => {
        data.forEach((post) => {
          this.posts.push(post);
        });
        console.log(this.posts);
      });
  },

  methods: {
    sort: function (payload) {
      if (payload.sorting === "up") {
        this.posts = this.posts.sort((a, b) => a.id - b.id);
        return this.posts;
      } else if (payload.sorting === "down") {
        this.posts = this.posts.sort((a, b) => b.id - a.id);
        return this.posts;
      } else if (payload.sorting === "random") {
        this.posts = this.posts.sort(() => Math.random() - 0.5);
        return this.posts;
      }
    },
  },
});
